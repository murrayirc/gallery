﻿using UnityEngine;
using System.Collections;

public class Floating : MonoBehaviour 
{
	Vector3 _startPos;

	[SerializeField] float _velocity = 5;
	[SerializeField] float _amplitude = 5;

	void Awake() 
	{
		_startPos = transform.position;
	}

	void Update() 
	{
		transform.position = new Vector3(transform.position.x,
		                                 _startPos.y + (_amplitude * Mathf.Sin(_velocity * Time.time)),
		               					 transform.position.z);
	}
}
