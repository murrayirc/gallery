﻿using UnityEngine;
using System.Collections;

public class LiftableObject : MonoBehaviour 
{
	public bool isSet = false;
	public Transform destination;

	Rigidbody _rb;
	Collider _col;

	void Awake()
	{
		_rb = GetComponent<Rigidbody>();
		_col = GetComponent<Collider>();
		if (!destination)
		{
			Debug.LogError("Liftable Object has no destination.");
		}
	}

	void Update()
	{
		if (transform.position.y < -10)
		{
			Debug.LogError("Warning: Liftable Object under world.");
		}
	}

	public void Lift()
	{
		_rb.isKinematic = true;
		_rb.useGravity = false;
		_col.isTrigger = true;
	}

	public void Drop()
	{
		_rb.isKinematic = false;
		_rb.useGravity = true;
		_col.isTrigger = false;
	}

	public void SetInPlace()
	{
		isSet = true;
		_rb.isKinematic = true;
		_rb.useGravity = false;
		transform.position = destination.transform.position;
		transform.rotation = destination.transform.rotation;
		transform.parent = destination;
	}

	public bool IsCloseToDestination(float minDistanceMagnitude)
	{
		float distance = (transform.position - destination.position).magnitude;

		return distance < minDistanceMagnitude;
	}
}
