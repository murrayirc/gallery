﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LiftableObject))]
public class RadioPlay : MonoBehaviour 
{
	LiftableObject _lo;
	AudioSource _audio;

	void Awake()
	{
		_lo = GetComponent<LiftableObject>();
		_audio = GetComponent<AudioSource>();
	}

	void Update()
	{
		if (_lo.isSet && !_audio.isPlaying)
		{
			_audio.Play();
		}
	}
}
