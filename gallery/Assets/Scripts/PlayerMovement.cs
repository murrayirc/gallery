﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{	
	Rigidbody rb;
	
	public float moveSpeed = 10.0f;
	public float gravity = 10.0f;
	public float maxVelocityChange = 10.0f;
	
	void Awake () 
	{
		rb = GetComponent<Rigidbody>();
		rb.freezeRotation = true;
		rb.useGravity = false;
	}
	
	void FixedUpdate () 
	{
		// Calculate how fast we should be moving
		// Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		targetVelocity = transform.TransformDirection(targetVelocity);
		targetVelocity *= moveSpeed;
		
		// Apply a force that attempts to reach our target velocity
		Vector3 velocity = rb.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = 0;
		rb.AddForce(velocityChange, ForceMode.VelocityChange);
		
		// We apply gravity manually for more tuning control
		rb.AddForce(new Vector3 (0, -gravity * rb.mass, 0));
	}
}
