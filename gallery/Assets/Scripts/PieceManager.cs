﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PieceManager : MonoBehaviour 
{
	// The following fields MUST be set in the inspector.
	[SerializeField] GameObject _player;
	[SerializeField] GameObject _canvas;

	[SerializeField] float _minDistanceMagnitude = 2.75f;

	void Awake()
	{
		if (!_player || !_canvas)
		{
			Debug.LogError("One or more fields is missing the necesarry components it needs to run.");
		}
	}
	
	void Update()
	{
		if (PlayerInVicinityWithPiece())
		{
			if (!UIManager.Inst.GetSpaceBarEnabled())
			{
				UIManager.Inst.SetSpaceBarEnabled(true);
			}

			if (Input.GetKeyDown (KeyCode.Space))
			{
				LiftableObject playerLiftedObject = _player.GetComponentInChildren<PlayerLiftObjects>().liftedObject;
				playerLiftedObject.SetInPlace();
				_player.GetComponent<PlayerLiftObjects>().ObjectSetInPlace();
			}
		}
		else
		{
			if (UIManager.Inst.GetSpaceBarEnabled())
			{
				UIManager.Inst.SetSpaceBarEnabled(false);
			}
		}
	}

	bool PlayerInVicinityWithPiece()
	{
		if (PlayerHasPiece())
		{
			return _player.GetComponentInChildren<PlayerLiftObjects>().liftedObject.IsCloseToDestination(_minDistanceMagnitude);
		}
		else
		{
			return false;
		}
	}

	bool PlayerHasPiece()
	{
		return _player.GetComponentInChildren<PlayerLiftObjects>().liftedObject;
	}
}
