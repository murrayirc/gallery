﻿using UnityEngine;
using System.Collections;

public class PlayerLiftObjects : MonoBehaviour 
{
	Camera _cam;
	bool _isLifting;

	public LiftableObject liftedObject;

	LiftTarget _liftTarget;

	[SerializeField] float _followSmoothing = 5f;

	[SerializeField] float _requiredProximity = 5f;

	void Awake()
	{
		_liftTarget = GetComponentInChildren<LiftTarget>();
		_cam = Camera.main;
		_isLifting = false;
	}

	void Update()
	{
		LiftAndDrop();
		CarryObject();
	}

	void LiftAndDrop()
	{
		// Lifting
		if (Input.GetMouseButtonDown(0) && !_isLifting)
		{
			Ray camRay = new Ray(_cam.transform.position, _cam.transform.forward);
			RaycastHit camRayHit;

			if (Physics.Raycast(camRay, out camRayHit, _requiredProximity))
			{
				if (camRayHit.transform.GetComponent<LiftableObject>())
				{
					if (!camRayHit.transform.gameObject.GetComponent<LiftableObject>().isSet)
					{
						liftedObject = camRayHit.transform.gameObject.GetComponent<LiftableObject>();
						liftedObject.Lift();
						_isLifting = true;
					}
				}
			}
		}

		// Dropping
		else if (Input.GetMouseButtonDown(0) && _isLifting)
		{
			liftedObject.Drop();
			liftedObject = null;
			_isLifting = false;
		}
	}

	void CarryObject()
	{
		if (!_isLifting || !liftedObject || liftedObject.isSet)
		{
			return;
		}

		Quaternion targetRotation = Quaternion.LookRotation(liftedObject.transform.position - _cam.transform.position);
		liftedObject.transform.localRotation = Quaternion.Slerp(liftedObject.transform.localRotation,
		                                                        targetRotation,
		                                                        (1 / _followSmoothing));

		liftedObject.transform.position = _liftTarget.transform.position;
		
		//liftedObject.transform.position = Vector3.Lerp(liftedObject.transform.position, 
		//                                               _liftTarget.transform.position, 
		//                                               (1 / _followSmoothing));

		if (liftedObject.transform.position.y <= liftedObject.transform.localScale.y / 2f)
		{
			liftedObject.transform.position = new Vector3(liftedObject.transform.position.x,
			                                              liftedObject.transform.localScale.y / 2f,
			                                              liftedObject.transform.position.z);
		}
	}

	public void ObjectSetInPlace()
	{
		liftedObject = null;
		_isLifting = false;
	}
}
