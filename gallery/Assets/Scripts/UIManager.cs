﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
	public static UIManager _inst;
	public static UIManager Inst
	{
		get
		{
			if (_inst == null)
			{
				_inst = GameObject.FindObjectOfType<UIManager>();
			}
			return _inst;
		}
	}

	[SerializeField] GameObject _spaceBar;
	[SerializeField] Text _titleText;

	public bool GetSpaceBarEnabled() { return Inst_GetSpaceBarEnabled(); }
	bool Inst_GetSpaceBarEnabled() { return _spaceBar.GetComponent<RawImage>().enabled; }

	public void SetSpaceBarEnabled(bool setValue) { Inst_SetSpaceBarEnabled(setValue); }
	void Inst_SetSpaceBarEnabled(bool setValue)
	{
		_spaceBar.GetComponent<RawImage>().enabled = setValue;
	}

	void Awake()
	{
		Invoke ("DisableTitleText", 5);
	}

	void DisableTitleText()
	{
		_titleText.enabled = false;
	}
}
